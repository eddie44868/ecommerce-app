import 'package:build_angga2/pages/address.dart';
import 'package:build_angga2/pages/edit_profile.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../widgets/categories.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff030E22),
      body: SafeArea(
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
                  children: [
            const SizedBox(
              height: 20,
            ),
            Center(
              child: Text(
                "Profile",
                style: GoogleFonts.montserrat(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Container(
              width: 150,
              height: 150,
              decoration: BoxDecoration(
                color: const Color(0xff2C3545),
                borderRadius: BorderRadius.circular(100),
                border: const Border(
                  left: BorderSide(
                    color: Color(0xff6C5ECF),
                  ),
                  right: BorderSide(
                    color: Color(0xff6C5ECF),
                  ),
                  top: BorderSide(
                    color: Color(0xff6C5ECF),
                  ),
                  bottom: BorderSide(
                    color: Color(0xff6C5ECF),
                  ),
                ),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: Image.asset(
                  "asset/300.jpg",
                  width: 150,
                  height: 150,
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Center(
              child: Text(
                "John Doe",
                style: GoogleFonts.montserrat(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 32, left: 16, right: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Order History",
                    style: GoogleFonts.montserrat(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "See All",
                    style: GoogleFonts.montserrat(
                        color: const Color(0xffCFCFCF),
                        fontSize: 13,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Categories(
                    text: " Pending",
                    imageUrl: Icons.assignment,
                  ),
                  Categories(text: "Packed", imageUrl: Icons.add_box),
                  Categories(
                    text: "On The Way",
                    imageUrl: Icons.bus_alert,
                  ),
                  Categories(
                    text: "Arrived",
                    imageUrl: Icons.check_rounded,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 16, right: 16, bottom: 17),
              child: InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const EditProfile()));
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Edit Profile",
                      style: GoogleFonts.montserrat(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                    const Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.white,
                    )
                  ],
                ),
              ),
            ),
            const Divider(
              color: Color(0xff707070),
              thickness: 1,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 16, right: 16, bottom: 17),
              child: InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const Address()));
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Address",
                      style: GoogleFonts.montserrat(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                    const Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.white,
                    )
                  ],
                ),
              ),
            ),
            const Divider(
              color: Color(0xff707070),
              thickness: 1,
            ),
            const SizedBox(
                  height: 15,
                ),
            Padding(
              padding: const EdgeInsets.only(bottom: 20),
              child: SizedBox(
                width: 340,
                height: 47,
                child: TextButton(
                  style: TextButton.styleFrom(
                      backgroundColor: const Color(0xff6C5ECF),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18))),
                  onPressed: () {},
                  child: Text(
                    "Logout",
                    style: GoogleFonts.montserrat(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),

                  ],
                ),
          )),
    );
  }
}
