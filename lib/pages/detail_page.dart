import 'package:build_angga2/pages/cart_page.dart';
import 'package:build_angga2/pages/order_detail.dart';
import 'package:build_angga2/widgets/product_list.dart';
import 'package:build_angga2/widgets/variation.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class DetailPage extends StatelessWidget {
  const DetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color(0xff030E22),
        body: SafeArea(
            child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 15, left: 15, right: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          color: const Color(0xff2C3545),
                          borderRadius: BorderRadius.circular(13),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            Icon(
                              Icons.arrow_back,
                              size: 25,
                              color: Colors.white,
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const Cart()));
                      },
                      child: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          color: const Color(0xff2C3545),
                          borderRadius: BorderRadius.circular(13),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              "asset/ker.png",
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Image.asset(
                "asset/adidas.png",
                width: 375,
                height: 385,
              ),
              Image.asset(
                "asset/dots.png",
                width: 40,
                height: 20,
                color: Colors.white,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 34, left: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Nike Air Force x",
                      style: GoogleFonts.montserrat(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 6,
                    ),
                    Row(
                      children: [
                        Image.asset(
                          'asset/star.png',
                          width: 88,
                          height: 16,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Text(
                          "(16)",
                          style: GoogleFonts.montserrat(
                              color: const Color(0xffCFCFCF),
                              fontSize: 12,
                              fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Row(
                      children: [
                        Text(
                          "Rp1.650.000",
                          style: GoogleFonts.montserrat(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(
                          width: 6,
                        ),
                        Text(
                          "Rp3.650.000",
                          style: GoogleFonts.montserrat(
                            color: const Color(0xffCFCFCF),
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            decoration: TextDecoration.lineThrough,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 32,
                    ),
                    Text(
                      'Choose Variations',
                      style: GoogleFonts.montserrat(
                        color: const Color(0xffCFCFCF),
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 16, right: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: 59,
                            height: 59,
                            decoration: BoxDecoration(
                              color: const Color(0xff2C3545),
                              borderRadius: BorderRadius.circular(24),
                              border: const Border(
                                left: BorderSide(
                                  color: Color(0xff6C5ECF),
                                ),
                                right: BorderSide(
                                  color: Color(0xff6C5ECF),
                                ),
                                top: BorderSide(
                                  color: Color(0xff6C5ECF),
                                ),
                                bottom: BorderSide(
                                  color: Color(0xff6C5ECF),
                                ),
                              ),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "36",
                                  style: GoogleFonts.montserrat(
                                    color: const Color(0xffCFCFCF),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Variation(
                            number: '37',
                          ),
                          Variation(
                            number: '38',
                          ),
                          Variation(
                            number: '39',
                          ),
                          Variation(
                            number: '40',
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 24,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Description",
                          style: GoogleFonts.montserrat(
                            color: const Color(0xffCFCFCF),
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        Text(
                          "This shoes material is canvas press with foam \nmat, bring back your high school moment with \nthis shoes. Choose size and just wait for it.",
                          style: GoogleFonts.montserrat(
                            color: const Color(0xffCFCFCF),
                          ),
                        ),
                        const SizedBox(
                          height: 24,
                        ),
                        Text(
                          "Related Product",
                          style: GoogleFonts.montserrat(
                            color: const Color(0xffCFCFCF),
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ProductList(
                              imageUrl: "asset/shoes1.png",
                              rating: "asset/star.png",
                              amount: '(17)',
                              price: "Rp750.000",
                              name: 'Nike Bounces',
                            ),
                            ProductList(
                              imageUrl: "asset/shoes2.png",
                              rating: "asset/star.png",
                              amount: '(14)',
                              price: "Rp550.000",
                              name: 'Nike Eight',
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                ProductList(
                                  imageUrl: "asset/shoes3.png",
                                  rating: "asset/star.png",
                                  amount: '(15)',
                                  price: "Rp600.000",
                                  name: 'Nike Dunk',
                                ),
                                ProductList(
                                  imageUrl: "asset/oren.png",
                                  rating: "asset/star.png",
                                  amount: '(16)',
                                  price: "Rp400.000",
                                  name: 'Nike Flat',
                                ),
                              ],
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                ProductList(
                                  imageUrl: "asset/shoes.png",
                                  rating: "asset/star.png",
                                  amount: '(15)',
                                  price: "Rp550.000",
                                  name: 'Adidas alpha',
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 32,
                            ),
                            Text(
                              "You've reached the end",
                              style: GoogleFonts.montserrat(
                                color: Colors.white,
                                fontSize: 12,
                              ),
                            ),
                            const SizedBox(
                              height: 100,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        )),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.only(left: 15, right: 15),
          child: BottomNavigationBar(
              backgroundColor: const Color(0xff030E22),
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Text(
                    "Rp1.650.000",
                    style: GoogleFonts.montserrat(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                  label: "",
                ),
                BottomNavigationBarItem(
                  icon: Image.asset(
                    "asset/ker.png",
                    width: 25,
                    height: 25,
                  ),
                  label: "",
                ),
                BottomNavigationBarItem(
                  icon: SizedBox(
                    width: 153,
                    height: 47,
                    child: TextButton(
                      style: TextButton.styleFrom(
                          backgroundColor: const Color(0xff6C5ECF),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18))),
                      onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const OrderDetail()));
                      },
                      child: Text(
                        "Buy Now",
                        style: GoogleFonts.montserrat(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  label: "",
                )
              ]),
        ));
  }
}
