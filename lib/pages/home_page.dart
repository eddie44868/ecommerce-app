import 'package:build_angga2/pages/cart_page.dart';
import 'package:build_angga2/pages/detail_page.dart';
import 'package:build_angga2/pages/search_page.dart';
import 'package:build_angga2/widgets/categories.dart';
import 'package:build_angga2/widgets/product_list.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Home extends StatelessWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff030E22),
      body: SafeArea(
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 30, left: 15, right: 15),
                child: Row(
                  children: [
                    Flexible(
                      child: SizedBox(
                        height: 40,
                        width: 295,
                        child: TextFormField(
                          cursorColor: Colors.white,
                          decoration: InputDecoration(
                              contentPadding:
                                  const EdgeInsets.only(top: 11, bottom: 11),
                              fillColor: const Color(0xff2C3545),
                              filled: true,
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(21),
                                borderSide: BorderSide.none,
                              ),
                              hintText: "I'm Searching for...",
                              hintStyle: GoogleFonts.montserrat(
                                color: const Color.fromARGB(255, 118, 108, 108),
                              ),
                              prefixIcon: IconButton(
                                icon: const Icon(
                                  Icons.search,
                                  color: Color.fromARGB(255, 118, 108, 108),
                                ), onPressed: () {
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => const SearchPage()));
                                },
                              )),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const Cart()));
                      },
                      child: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          color: const Color(0xff2C3545),
                          borderRadius: BorderRadius.circular(13),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              "asset/ker.png",
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Padding(
                  padding: const EdgeInsets.only(top: 24, left: 16),
                  child: Row(
                    children: [
                      Container(
                        width: 294,
                        height: 148,
                        decoration: BoxDecoration(
                          color: const Color(0xff6C5ECF),
                          borderRadius: BorderRadius.circular(17),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 24, top: 27),
                          child: Row(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("New Arrival \nItem Up to 30%",
                                      style: GoogleFonts.montserrat(
                                          color: Colors.white,
                                          fontSize: 19,
                                          fontWeight: FontWeight.w700)),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  OutlinedButton(
                                    style: OutlinedButton.styleFrom(
                                        side: const BorderSide(color: Colors.white),
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(17),
                                        )),
                                    onPressed: () {},
                                    child: Text("Grab It Now",
                                        style: GoogleFonts.montserrat(
                                            color: Colors.white,
                                            fontSize: 12,
                                            fontWeight: FontWeight.w500)),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    bottom: 16, right: 17),
                                child: Image.asset(
                                  "asset/smart.png",
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 16,
                      ),
                      Container(
                        width: 294,
                        height: 148,
                        decoration: BoxDecoration(
                          color: const Color(0xff21AE7B),
                          borderRadius: BorderRadius.circular(17),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 24, top: 27),
                          child: Row(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Flash Sale \n12.12",
                                      style: GoogleFonts.montserrat(
                                          color: Colors.white,
                                          fontSize: 19,
                                          fontWeight: FontWeight.w700)),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  OutlinedButton(
                                    style: OutlinedButton.styleFrom(
                                        side: const BorderSide(color: Colors.white),
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(17),
                                        )),
                                    onPressed: () {},
                                    child: Text("Grab It Now",
                                        style: GoogleFonts.montserrat(
                                            color: Colors.white,
                                            fontSize: 12,
                                            fontWeight: FontWeight.w500)),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    right: 10, left: 45, bottom: 10),
                                child: Image.asset(
                                  "asset/headphone.png",
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 32, left: 16, right: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Top Categories",
                      style: GoogleFonts.montserrat(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      "See All",
                      style: GoogleFonts.montserrat(
                          color: const Color(0xffCFCFCF),
                          fontSize: 13,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Categories(
                      text: "Stationary",
                      imageUrl: Icons.assignment,
                    ),
                    Categories(
                        text: "Electronic",
                        imageUrl: Icons.camera_roll_outlined),
                    Categories(
                      text: "Houseware",
                      imageUrl: Icons.business_center,
                    ),
                    Categories(
                      text: "Collection",
                      imageUrl: Icons.headphones,
                    ),
                  ],
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 16, top: 16),
                    child: Text(
                      "Something You Like",
                      style: GoogleFonts.montserrat(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 20, left: 15, right: 10, bottom: 50),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => const DetailPage()));
                          },
                          child: ProductList(
                            imageUrl: "asset/adidas.png",
                            rating: "asset/star.png",
                            amount: '(16)',
                            price: "Rp1.650.000",
                            name: 'Nike Air Force X',
                          ),
                        ),
                        ProductList(
                          imageUrl: "asset/smart.png",
                          rating: "asset/star.png",
                          amount: '(15)',
                          price: "Rp2.550.000",
                          name: 'Smartwatch 2.0',
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ProductList(
                          imageUrl: "asset/airpods.png",
                          rating: "asset/star.png",
                          amount: '(17)',
                          price: "Rp650.000",
                          name: 'Airpods',
                        ),
                        ProductList(
                          imageUrl: "asset/headphone.png",
                          rating: "asset/star.png",
                          amount: '(14)',
                          price: "Rp1.550.000",
                          name: 'Rexus Headphone',
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ProductList(
                          imageUrl: "asset/face.png",
                          rating: "asset/star.png",
                          amount: '(15)',
                          price: "Rp50.000",
                          name: 'Garnier Facewash',
                        ),
                        ProductList(
                          imageUrl: "asset/lamp.png",
                          rating: "asset/star.png",
                          amount: '(15)',
                          price: "Rp40.000",
                          name: 'LED Wifi Lamp',
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 32,
                    ),
                    Text(
                      "You've reached the end",
                      style: GoogleFonts.montserrat(
                        color: Colors.white,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
