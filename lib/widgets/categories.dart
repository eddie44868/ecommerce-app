import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

// ignore: must_be_immutable
class Categories extends StatelessWidget {
  String text;
  IconData? imageUrl;

  Categories({Key? key, this.imageUrl, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 64,
          height: 64,
          decoration: BoxDecoration(
            color: const Color(0xff2C3546),
            borderRadius: BorderRadius.circular(24),
          ),
          child: Icon(
            imageUrl,
            color: Colors.white,
          ),
        ),
        const SizedBox(
          height: 8,
        ),
        Text(
          text,
          style: GoogleFonts.montserrat(
            color: Colors.white,
            fontSize: 12,
          ),
        ),
      ],
    );
  }
}
