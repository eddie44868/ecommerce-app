import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

// ignore: must_be_immutable
class FormTile extends StatelessWidget {
  String text;
  String hintText;

  FormTile({Key? key, required this.text, required this.hintText}) : super(key: key);



  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          text,
          style: GoogleFonts.montserrat(
              color: Colors.white, fontSize: 14, fontWeight: FontWeight.w500),
        ),
        const SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 40,
          width: 295,
          child: TextFormField(
            cursorColor: Colors.white,
            style: GoogleFonts.montserrat(
              color: Colors.white,
              fontSize: 14,
            ),
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.only(left: 20, top: 11),
              fillColor: const Color(0xff2C3545),
              filled: true,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(21),
                borderSide: BorderSide.none,
              ),
              hintText: hintText,
              hintStyle: GoogleFonts.montserrat(color: const Color(0xff68687A)),
            ),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }
}
