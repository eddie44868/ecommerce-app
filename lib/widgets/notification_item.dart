import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

// ignore: must_be_immutable
class NotificationItem extends StatelessWidget {
  String title;
  String text;
  String time;

  NotificationItem({Key? key, 
    required this.title,
    required this.text,
    required this.time,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: GoogleFonts.montserrat(
                    color: const Color(0xff6C5ECF), fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 8,
              ),
              Text(
                text,
                style: GoogleFonts.montserrat(
                  color: const Color(0xffCFCFCF),
                  fontSize: 12,
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              Row(
                children: [
                  const Icon(
                    Icons.timelapse,
                    color: Color(0xff8A99AB),
                    size: 16,
                  ),
                  const SizedBox(
                    width: 4,
                  ),
                  Text(
                    time,
                    style: GoogleFonts.montserrat(
                        color: const Color(0xff8A99AB), fontSize: 10),
                  )
                ],
              ),
              const SizedBox(
                height: 17,
              ),
            ],
          ),
        ),
        const Divider(
          color: Color(0xff707070),
          thickness: 1,
        )
      ],
    );
  }
}
