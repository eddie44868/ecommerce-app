import 'package:build_angga2/widgets/form_tile.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class EditAddress extends StatelessWidget {
  const EditAddress({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: const Color(0xff030E22),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: SafeArea(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20, left: 15, right: 15,),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          color: const Color(0xff2C3545),
                          borderRadius: BorderRadius.circular(13),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            Icon(
                              Icons.arrow_back,
                              size: 25,
                              color: Colors.white,
                            )
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    Text(
                      "Edit Address",
                      style: GoogleFonts.montserrat(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    Container(
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                        color: const Color(0xff030E22),
                        borderRadius: BorderRadius.circular(13),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Column(
                children: [
                  FormTile(text: "Address", hintText: "Address"),
                  FormTile(text: "Name", hintText: "Name"),
                  FormTile(text: "Phone Number", hintText: "Phone Number"),
                  FormTile(text: "Road Name - House Number", hintText:"Road Name - House Number"),
                  FormTile(text: "Province", hintText: "Province"),
                  FormTile(text: "City", hintText: "City"),
                  FormTile(text: "Subdistrict", hintText:"Subdistrict"),
                  FormTile(text: "Pos Number", hintText: "Pos Number"),
                  FormTile(text: "Additional Details", hintText: "Additional Details"),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30, bottom: 15),
                child: SizedBox(
                  width: 300,
                  height: 47,
                  child: TextButton(
                    style: TextButton.styleFrom(
                        backgroundColor: const Color(0xff6C5ECF),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18))),
                    onPressed: () {
                     
                    },
                    child: Text(
                      "Save",
                      style: GoogleFonts.montserrat(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
              TextButton(
                onPressed: (){
                   Navigator.pop(context);
                },
                child: Text(
                  "Discard",
                  style: GoogleFonts.montserrat(
                      color: const Color(0xff6C5ECF),
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
            ],
          )
          ),
      ),
    );
  }
}