import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

// ignore: must_be_immutable
class ProductList extends StatelessWidget {
  String imageUrl;
  String name;
  String rating;
  String price;
  String amount;

  ProductList({Key? key, required this.imageUrl, required this.name, required this.rating, required this.price, required this.amount}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              width: 160,
              height: 250,
              decoration: BoxDecoration(
                color: const Color(0xff2C3545),
                borderRadius: BorderRadius.circular(28),
              ),
              child: Padding(
                padding: const EdgeInsets.only(top: 8, left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset(imageUrl,
                    width: 130,
                    height: 130,
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          name,
                          style: GoogleFonts.montserrat(
                              color: Colors.white,
                              fontSize: 14,
                              fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(
                          height: 4,
                        ),
                        Row(
                          children: [
                            Image.asset(
                              rating,
                              width: 88,
                              height: 16,
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            Text(
                              amount,
                              style: GoogleFonts.montserrat(
                                  color: const Color(0xffCFCFCF),
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Text(
                          price,
                          style: GoogleFonts.montserrat(
                              color: const Color(0xffCFCFCF),
                              fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ],
    );
  }
}
