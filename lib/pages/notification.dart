import 'package:build_angga2/widgets/notification_item.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class NotificationPage extends StatelessWidget {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff030E22),
      body: SafeArea(
          child: Column(
        children: [
          const SizedBox(height: 20,),
          Center(
            child: Text(
              "Notification",
              style: GoogleFonts.montserrat(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.bold
              ),
            ),
          ),
           const SizedBox(height: 35,),
          NotificationItem(
            title: "Order #242435657674 has arrived!", 
            text: "What you've been waiting has arrived! Don't forget \nto confirm on the history page", 
            time: "09-04-2021 17:51"),
          NotificationItem(
            title: "Order #242435657271 has been canceled!", 
            text: "Your order has been canceled by the system. Chat to \nthe shop owner for more information", 
            time: "22-05-2021 15:40"),
          NotificationItem(
            title: "Order #242435657573 has arrived!", 
            text: "What you've been waiting has arrived! Don't forget \nto confirm on the history page", 
            time: "12-06-2021 12:30"),
          NotificationItem(
            title: "Order #242435658254 has arrived!", 
            text: "What you've been waiting has arrived! Don't forget \nto confirm on the history page", 
            time: "11-08-2021 14:21"),
        ],
      )),
    );
  }
}
