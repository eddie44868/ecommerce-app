import 'package:build_angga2/pages/notification.dart';
import 'package:build_angga2/pages/profile_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'home_page.dart';
import 'product_page.dart';

class BottomNav extends StatefulWidget {
  const BottomNav({ Key? key }) : super(key: key);
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  @override
  State<BottomNav> createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> {
int _selectedIndex = 0;
final halaman = [
    const Home(),
    const ProductPage(),
    const NotificationPage(),
    const ProfilePage()
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: halaman[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: const Color(0xff030E22),
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(top: 16, bottom: 8),
              child: Icon(
                Icons.home,
              ),
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(top: 16, bottom: 8),
              child: Icon(Icons.backup_table),
            ),
            label: 'Product',
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(top: 16, bottom: 8),
              child: Icon(Icons.notifications),
            ),
            label: 'Notification',
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(top: 16, bottom: 8),
              child: Icon(Icons.account_circle_rounded),
            ),
            label: 'Profile',
          ),
        ],
        selectedLabelStyle: GoogleFonts.montserrat(
          color: Colors.white,
          fontSize: 10,
        ),
        unselectedLabelStyle: GoogleFonts.montserrat(
          color: const Color(0xffCFCFCF),
          fontSize: 10,
        ),
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.grey,
        onTap: _onItemTapped,
      )
    );
  }
}