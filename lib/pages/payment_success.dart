import 'package:build_angga2/pages/bottom_nav.dart';
import 'package:build_angga2/pages/order_history.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class PaymentSuccess extends StatelessWidget {
  const PaymentSuccess({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff030E22),
      body: SafeArea(
          child: Column(
        children: [
          const SizedBox(
            height: 147,
          ),
          Center(
            child: Image.asset(
              "asset/bags.png",
              width: 200,
              height: 200,
            ),
          ),
          const SizedBox(
            height: 50,
          ),
          Text(
            "Payment Success",
            style: GoogleFonts.montserrat(
                color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
          ),
          const SizedBox(
            height: 19,
          ),
          Text(
            "Hoooorayy! Your payment was successful!!\nJust wait for it soon!",
            style: GoogleFonts.montserrat(
                color: const Color(0xffCFCFCF),
                fontSize: 14,
                fontWeight: FontWeight.w500),
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 40,
          ),
          SizedBox(
            width: 300,
            height: 47,
            child: TextButton(
              style: TextButton.styleFrom(
                  backgroundColor: const Color(0xff6C5ECF),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18))),
              onPressed: () {
               Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const OrderHistory()));
              },
              child: Text(
                "Track Order Status",
                style: GoogleFonts.montserrat(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          TextButton(
            onPressed: (){
               Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const BottomNav()));
            },
            child: Text(
              "Back To Home",
              style: GoogleFonts.montserrat(
                  color: const Color(0xff6C5ECF),
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ],
      )),
    );
  }
}
