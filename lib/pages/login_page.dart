import 'package:build_angga2/pages/bottom_nav.dart';
import 'package:build_angga2/pages/register.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: const Color(0xff030E22),
      body: Padding(
        padding: const EdgeInsets.only(left: 40, right: 40, top: 138),
        child: Column(
          children: [
            Center(
              child: Image.asset(
                "asset/shop.png",
                width: 90,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            Text(
              "Let's Sign You In.",
              style: GoogleFonts.montserrat(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 40,
            ),
            SizedBox(
              height: 40,
              width: 295,
              child: TextFormField(
                cursorColor: Colors.white,
                style: GoogleFonts.montserrat(
                  color: Colors.white,
                  fontSize: 14,
                ),
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.only(left: 20, top: 11),
                  fillColor: const Color(0xff2C3545),
                  filled: true,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(21),
                    borderSide: BorderSide.none,
                  ),
                  hintText: 'Email',
                  hintStyle: GoogleFonts.montserrat(color: const Color(0xff68687A)),
                ),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            SizedBox(
              height: 40,
              width: 295,
              child: TextFormField(
                cursorColor: Colors.white,
                obscureText: true,
                style: GoogleFonts.montserrat(
                  color: Colors.white,
                  fontSize: 14,
                ),
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.only(left: 20, top: 11),
                  fillColor: const Color(0xff2C3545),
                  filled: true,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(21),
                    borderSide: BorderSide.none,
                  ),
                  hintText: 'Password',
                  hintStyle: GoogleFonts.montserrat(color: const Color(0xff68687A)),
                ),
              ),
            ),
            const SizedBox(
              height: 17,
            ),
            Row(
              children: [
                Image.asset(
                  "asset/rec.png",
                  width: 20,
                ),
                const SizedBox(
                  width: 10,
                ),
                Text(
                  "Remember Me",
                  style: GoogleFonts.montserrat(
                    color: Colors.white,
                    fontSize: 12,
                  ),
                ),
                const SizedBox(
                  width: 66,
                ),
                Text(
                  "Forgot Password?",
                  style: GoogleFonts.montserrat(
                    color: Colors.white,
                    fontSize: 12,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 39,
            ),
            SizedBox(
              width: 295,
              height: 47,
              child: TextButton(
                  style: TextButton.styleFrom(
                      backgroundColor: const Color(0xff6C5ECF),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18))),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const BottomNav()));
                  },
                  child: Text(
                    "Login",
                    style: GoogleFonts.montserrat(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  )),
            ),
            const SizedBox(
              height: 39,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Don't have account?",
                  style: GoogleFonts.montserrat(
                    color: Colors.white,
                    fontSize: 14,
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                InkWell(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const RegisterPage()));
                  },
                  child: Text(
                    "Register",
                    style: GoogleFonts.montserrat(
                        color: const Color(0xff6C5ECF),
                        fontSize: 14,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
