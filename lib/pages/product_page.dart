import 'package:build_angga2/pages/cart_page.dart';
import 'package:build_angga2/pages/detail_page.dart';
import 'package:build_angga2/widgets/product_list.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ProductPage extends StatelessWidget {
  const ProductPage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff030E22),
      body: SafeArea(
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 30, left: 15, right: 15),
                child: Row(
                  children: [
                    Flexible(
                      child: SizedBox(
                        height: 40,
                        width: 295,
                        child: TextFormField(
                          cursorColor: Colors.white,
                          decoration: InputDecoration(
                              contentPadding:
                                  const EdgeInsets.only(top: 11, bottom: 11),
                              fillColor: const Color(0xff2C3545),
                              filled: true,
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(21),
                                borderSide: BorderSide.none,
                              ),
                              hintText: "I'm Searching for...",
                              hintStyle: GoogleFonts.montserrat(
                                color: const Color.fromARGB(255, 118, 108, 108),
                              ),
                              prefixIcon: const Icon(
                                Icons.search,
                                color: Color.fromARGB(255, 118, 108, 108),
                              )),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const Cart()));
                      },
                      child: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          color: const Color(0xff2C3545),
                          borderRadius: BorderRadius.circular(13),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              "asset/ker.png",
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 20, left: 15, right: 10, bottom: 50),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => const DetailPage()));
                          },
                          child: ProductList(
                            imageUrl: "asset/adidas.png",
                            rating: "asset/star.png",
                            amount: '(16)',
                            price: "Rp1.650.000",
                            name: 'Nike Air Force X',
                          ),
                        ),
                        ProductList(
                          imageUrl: "asset/smart.png",
                          rating: "asset/star.png",
                          amount: '(15)',
                          price: "Rp2.550.000",
                          name: 'Smartwatch 2.0',
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ProductList(
                          imageUrl: "asset/airpods.png",
                          rating: "asset/star.png",
                          amount: '(17)',
                          price: "Rp650.000",
                          name: 'Airpods',
                        ),
                        ProductList(
                          imageUrl: "asset/headphone.png",
                          rating: "asset/star.png",
                          amount: '(14)',
                          price: "Rp1.550.000",
                          name: 'Rexus Headphone',
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ProductList(
                          imageUrl: "asset/face.png",
                          rating: "asset/star.png",
                          amount: '(15)',
                          price: "Rp50.000",
                          name: 'Garnier Facewash',
                        ),
                        ProductList(
                          imageUrl: "asset/lamp.png",
                          rating: "asset/star.png",
                          amount: '(15)',
                          price: "Rp40.000",
                          name: 'LED Wifi Lamp',
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 32,
                    ),
                    Text(
                      "You've reached the end",
                      style: GoogleFonts.montserrat(
                        color: Colors.white,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}